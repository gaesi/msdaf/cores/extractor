<?php
// Primeiro descomente as linhas 
// extension=oci8_12c  ; Use with Oracle Database 12c Instant Client
// extension=pdo_oci

function formatDate($time) {
	return date('d/m/Y H:i:s', $time);
}

function maxMonthDay($year, $month) {
	$datetime = strtotime('last day of this month', strtotime("$year-$month-01"));
	return date('d', $datetime);
}

function query() {
	//$lastDay = maxMonthDay($year, $month);
	//$monthQuery = BASE_QUERY . "WHERE \"DT_DISPENSACAO\" >= DATE '$year-$month-01' AND \"DT_DISPENSACAO\" <= DATE '$year-$month-$lastDay' AND CO_ORIGEM_REGISTRO LIKE 'F' ORDER BY CO_SEQ_DISPENSACAO ASC;";
	return str_replace(';','', BASE_QUERY);
}

function process($sqlFileName) {
	// Connects to the XE service (i.e. database) on the "localhost" machine
	$usr = getenv("ORACLE_USR");
	$pwd = getenv('ORACLE_PWD');
	$conn = oci_connect($usr, $pwd, 'srvoradf0.saude.gov:1521/DFPOW1.SAUDE.GOV');
	if (!$conn) {
		$e = oci_error();
		trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
	}
	
	$firstRow=true;
	$csvFileDir='D:\outputs';
	$fpw = fopen("$csvFileDir\\$sqlFileName.csv", 'w');
	$stid = oci_parse($conn, query());
	
	oci_execute($stid);
	while($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
		if($firstRow) {
			$header = array_keys($row);
			fwrite($fpw,implode(";",$header)."\n");
		}
	
		fwrite($fpw,implode(";",$row)."\n");
		$firstRow=false;
	}
	
	fclose($fpw);
	oci_free_statement($stid);
	oci_close($conn);
}

$startTime = time();

$sqlFileName = 'bnafar-consolidado';
define("BASE_QUERY", file_get_contents("$sqlFileName.sql"));

$minMonth = 2;
$maxMonth = 12;
$initialYear = 2018;
$lastYear = 2018;

/*
//YearLoop
for($i=0; $i<=($lastYear-$initialYear); $i++) {
	$currentYear = $initialYear+$i;
	//Monthes Loop
	for ($m = $minMonth; $m <= $maxMonth; $m++) {
		process($sqlFileName, nextQuery($currentYear, $m), $currentYear, $m);
	}
}
*/

process($sqlFileName);
$endTime = time();

echo 'StartTime: ' . formatDate($startTime) . '<br>';
echo 'EndTime: ' . formatDate($endTime) . '<br>';

echo 'SpentTime: ' . ($endTime - $startTime) . 'seconds<br>';
echo 'SpentTime: ' . ($endTime - $startTime)/60 . 'minutes';

?>
