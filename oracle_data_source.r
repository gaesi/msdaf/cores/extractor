rm(list = ls())
library(RJDBC)
library(readr)
#library(hms)

#'sample'
#'counters'
#'extrator-2018'
#'VerificacaoDiscrepancia-MaioJunhoJulho-2019'
coordination = 'farmpop/'
sqlFileName = 'extrator-2018'
currentDir = dirname(rstudioapi::getSourceEditorContext()$path)

connectDB <- function() {
  DBUSR = Sys.getenv("ORACLE_USR")
  DBPWD = Sys.getenv("ORACLE_PWD")
  JDBC_DIR = Sys.getenv("JDBC")
  
  driver = JDBC(driverClass = "oracle.jdbc.OracleDriver", JDBC_DIR)
  ConURL = "jdbc:oracle:thin:@//srvoradf0.saude.gov:1521/DFPOW1.SAUDE.GOV"
  
  con = dbConnect(driver, ConURL, DBUSR, DBPWD)
  return <- con
}

getCounter <- function(con) {
  timeStart <- Sys.time()
  sqlFileNameDir = paste('/sqls/', coordination, 'counters','.sql', sep='')
  sqlFileDir = paste(currentDir, sqlFileNameDir, sep='')
  query = gsub(";", "", read_file(sqlFileDir))
  resp = dbGetQuery(con, query)
  timeEnd <- Sys.time()
  print(timeEnd - timeStart)
  
  return <- resp
}

getQuery <- function() {
  sqlFileNameDir = paste('/sqls/', coordination, sqlFileName,'.sql', sep='')
  sqlFileDir = paste(currentDir, sqlFileNameDir, sep='')
  query = gsub(";", "", read_file(sqlFileDir))
  return <- query
}

extractData <- function(con,query) {
  timeStart <- Sys.time()
  resp = dbGetQuery(con, query)
  time <- format(Sys.time(), "%Y-%m-%d-%Hh%Mm")
  #outputFile <-  paste(currentDir, '/outputs/', sqlFileName, '-', time, '.csv', sep='')
  outputFile <-  paste(currentDir, '/outputs/', sqlFileName, '.csv', sep='')
  
  #write.csv(resp, file = outputFile, append=TRUE)
  write.table(resp, file = outputFile, sep = ",", col.names = !file.exists(outputFile), append = T)
  
  timeEnd <- Sys.time()
  
  print(timeEnd - timeStart)  
}

limit = 19000
con = connectDB()
baseQuery = getQuery()
registers = getCounter(con)
registersAmount = registers[,'AMOUNT'] #2018-01 a 2018-02 = 19.945.996
print('registersAmount', registersAmount)

min = 1
max = as.integer(registersAmount/limit)+1

for(i in min:max) {
  print(i)
  currentQuery = paste(baseQuery, 'OFFSET ', i*limit, ' ROWS FETCH NEXT ', limit, ' ROWS ONLY', sep='')
  extractData(con,currentQuery)
}